# coding: utf-8
# kjcsvloader.py

import typing
import codecs
import pandas as pd
import yaml


class KJCsvLoader:
    def __init__(self, csvfile: str, has_header: bool = False, header_line: int = 0, coding: str ='cp932') -> None:
        super().__init__()
        self.csvfile = csvfile
        self.has_header = has_header
        self.header_line = header_line
        self.coding = coding
        self.df = None
        self.column_type_info = None

    def get_pd_header_info(self) -> typing.Union[None, int]:
        if self.has_header:
            return self.header_line
        else:
            return None

    def load(self) -> typing.NoReturn:
        if self.column_type_info is None:
            self.df = pd.read_csv(self.csvfile, header=self.get_pd_header_info(), 
                                  encoding=self.coding)
        else:
            self.df = pd.read_csv(self.csvfile, header=self.get_pd_header_info(), 
                                  encoding=self.coding,
                                  dtype=self.column_type_info)

    def column_names(self) -> list:
        return [col for col in self.df.columns]

    def column_types(self) -> list:
        return [col.name for col in self.df.dtypes]

    def get_column_types(self) -> dict:
        types = self.df.dtypes
        return dict(zip(self.column_names(), self.column_types()))

    def save_column_info(self, filename: str) -> typing.NoReturn:
        with codecs.open(filename, "w", encoding='utf-8') as f:
            yaml.safe_dump(self.get_column_types(), f, encoding='utf-8', allow_unicode=True, sort_keys=False)

    def load_column_info(self, filename: str) -> typing.NoReturn:
        with codecs.open(filename, "r", encoding='utf-8') as f:
            self.column_type_info = yaml.safe_load(f)

    def get_df(self) -> pd.DataFrame:
        if self.df is None:
            raise Exception('Error: Data is None.')
        else:
            return self.df

    def save_csv(self, filename:str, columns:typing.Union[None, list]=None, 
                 has_header:bool=True, encoding:str='cp932'):
        if columns is None:
            self.df.to_csv(filename, header=has_header, index=False, encoding=encoding)
        else:
            self.df.to_csv(filename, header=has_header, index=False, encoding=encoding, columns=columns)

    def count(self) -> int:
        if self.df is None:
            return 0
        else:
            return len(self.df)