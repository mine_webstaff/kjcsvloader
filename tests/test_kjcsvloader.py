import pytest
from kjcsvloader.kjcsvloader import KJCsvLoader
from collections import OrderedDict
import pandas as pd

def test_get_pd_header_info_header_non():
    assert KJCsvLoader('test.csv', has_header=False).get_pd_header_info() == None

def test_get_pd_header_info_topline_is_header():
    assert KJCsvLoader('test.csv', has_header=True).get_pd_header_info() == 0

def test_get_pd_header_info_secondline_is_header():
    assert KJCsvLoader('test.csv', has_header=True, header_line=1).get_pd_header_info() == 1

def test_load():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load()
    assert kcl.df.columns[0] == 'コース別順位'
    assert kcl.df.columns[1] == '全体順位'
    assert kcl.df.columns[2] == '受験番号'
    assert kcl.df.columns[3] == '専併'
    assert kcl.df.columns[24] == '特待備考'
    assert kcl.df.columns[25] == '他コース合格'

def test_column_names():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load()
    cols = kcl.column_names()
    assert cols[0] == 'コース別順位'
    assert cols[1] == '全体順位'
    assert cols[2] == '受験番号'
    assert cols[3] == '専併'
    assert cols[24] == '特待備考'
    assert cols[25] == '他コース合格'

def test_get_column_types():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load()
    col_typs = kcl.get_column_types()
    assert col_typs['コース別順位'] == 'float64'

def test_save_column_info():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load()
    kcl.save_column_info('tests/testdata/temp_kj_nyushi_column_info.yaml')

def test_load_column_info():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load_column_info('tests/testdata/kj_nyushi_column_info_setting.yaml')
    print(kcl.column_type_info)
    # assert kcl.column_type_info['コース別順位'] == 'int64'
    kcl.load()
    colinfo = kcl.get_column_types()
    assert colinfo['コース別順位'] == 'Int64'
    assert colinfo['出身学校コード'] == 'object'

def test_get_df_NoDataError():
    with pytest.raises(Exception) as e:
        kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
        df = kcl.get_df()
        assert e.value.message == 'Error: Data is None'

def test_get_df():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load_column_info('tests/testdata/kj_nyushi_column_info_setting.yaml')
    kcl.load()
    df = kcl.get_df()
    assert df.columns.size == 26
    assert df['コース別順位'].dtype == 'Int64'

def test_save_csv():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load_column_info('tests/testdata/kj_nyushi_column_info_setting.yaml')
    kcl.load()
    kcl.save_csv('tests/testdata/temp_kj_nyushi_data.csv')

def test_save_csv_columnSetting():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load_column_info('tests/testdata/kj_nyushi_column_info_setting.yaml')
    kcl.load()
    columns = ['受験番号','専併', 'コース', '氏名', '性別','出身学校コード','出身学校','５科合計']
    kcl.save_csv('tests/testdata/temp_kj_nyushi_data_column_select.csv',
                 columns=columns)

def test_count():
    kcl = KJCsvLoader('tests/testdata/kj_nyushi_test_data.csv', has_header=True)
    kcl.load_column_info('tests/testdata/kj_nyushi_column_info_setting.yaml')
    kcl.load()
    assert kcl.count() == 17
