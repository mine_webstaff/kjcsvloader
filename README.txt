kjcsvloader

賢者、ミライコンパスなどのCSVデータを取り扱いやすくするためのcsv loader

設定ファイル（v0.1.1ではYAML形式）でカラム情報（カラム名: 型名）を指定できる。

使い方:



relese version 0.1.1
from kjcsvloader import kjcsvloader

kcl = KJCsvLoader(ファイル名, has_header=True, coding='cp932')
ヘッダー行が２行目以降の場合は　header_line=1(0始まり）でヘッダー行を指定する。
この時点では、データのロードはしない。

kcl.load_column_info(設定ファイル名)　でカラム情報を指定できる。
カラム情報を指定しないときは、自動推定（pandas の自動推定）
NaN を含む 整数を指定するときは Int64 (int64だとNaNがあるとエラーになる)
NaN を含む 小数点付きデータは float64
文字列は object
にしておくと良い。

kcl.load() でデータを読み込む。

kcl.get_df() で　pandas.DataFrame 型でデータ出力
kcl.save_csv(ファイル名）で新たにCSVを出力
kcl.save_csv(ファイル名,colmuns=['カラム名1','カラム名5','カラム名2','カラム名4']) で
出力カラムを指定することができる。


v0.1.2.0
count()メソッド追加：データ数を返す。
kcl.count()



