import setuptools

def _requires_from_file(filename):
    return open(filename).read().splitlines()

setuptools.setup(
    name="kjcsvloader",
    version='0.1.2.0',
    description='csv easy loader for kenja.',
    author='Minematsu Toshiya',
    author_email='mine.webstaff@gmail.com',
    url='',
    packages=setuptools.find_packages(),
    install_requires=_requires_from_file('requirements.txt')
)
